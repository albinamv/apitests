# register/models
from faker import Faker

fake = Faker()


class RegisterUser:
    @staticmethod
    def random():
        email = fake.email()
        name = fake.name()
        password = fake.password()
        return {"email": email, "name": name, "password": password}
