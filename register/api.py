# register/api.py
# библиотека для отправки запросов
import requests
from jsonschema import validate
import logging

logger = logging.getLogger("api")


class Register:
    # конструктор класса - принимает url тестируемого приложения
    def __init__(self, url):
        self.url = url

    POST_REGISTER_USER = '/register'

    def register_user(self, body: dict, schema: dict):
        """
        https://testbase.atlassian.net/wiki/spaces/USERS/pages/592674928/doRegister
        """
        response = requests.post(f"{self.url}{self.POST_REGISTER_USER}", json=body)
        validate(instance=response.json(), schema=schema)
        logger.info(response.text)
        return response
