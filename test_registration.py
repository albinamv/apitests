from register.api import Register
from register.models import RegisterUser
from schemas.registration import valid_schema
import allure

URL = "http://users.bugred.ru/"
endpoint = "tasks/rest/doregister"


class TestRegistration:
    def test_registration(self):
        body = RegisterUser.random()

        response = Register(url=URL + endpoint).register_user(body=body, schema=valid_schema)

        with allure.step("Запрос отправлен, посмотрим код ответа"):
            assert response.status_code == 200

        with allure.step("Проверяем имя пользователя"):
            assert response.json().get('name') == body.get('name')

        with allure.step("Проверяем email пользователя"):
            assert response.json().get('email') == body.get('email')
