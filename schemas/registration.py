# schemas/registration.py
valid_schema = {
    "type": "object",
    "properties": {
        "name": {"type": "string"},
        "avatar": {"type": "string"},
        "password": {"type": "string"},
        # "birthday": {"type": "int"},
        "email": {"type": "string"},
        "gender": {"type": "string"},
        # "date_start": {"type": "string"},
        "hobby": {"type": "string"},
    },
    "required": ["email", "name", "password"]
}
